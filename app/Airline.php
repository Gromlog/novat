<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airline extends Model
{
	public $timestamps = false;
    public static function Insert($request){
    	$airline = new Airline;
    	$airline->name = $request['name'];
    	$airline->country = $request['country'];
    	$airline->save();
    }
    public static function Remove($request){
    	$remove = Airline::all()->where('id', $request['id'])->first();
    	$remove->delete();
    }
    public static function Detailed($request){
    	$airline = Airline::all()->where('id', $request['id'])->first();
    	return $airline;
    }
    public static function Updates($request){
    	$airline = Airline::all()->where('id', $request['id'])->first();
    	$airline->name = $request['name'];
    	$airline->country = $request['country'];
    	$airline->save();
   	}
}
