<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use FarhanWazir\GoogleMaps\GMaps;
use App\Country;
use App\Airline;
use App\Airport;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('home');
    }
    public function AirportView(){
        $GMaps = new Gmaps;
        $config['center'] = '54.687157, 25.279652';
        $config['zoom'] = '4';
        $GMaps->initialize($config);
        $map = $GMaps->create_map();
        return view('airportform')->with('map', $map)->with('countries', Country::all())->with('airlines', Airline::all());
    }
    public function NewAirport(Request $request){
        $this -> validate($request, [
            'name' => 'required',
            'location_x' => 'required',
            'airline' => 'required',
        ]);
        Airport::Insert($request);
        return redirect()->back()->with('success','Airport has been registered');
    }

    public function AirportListView(){
        return view('airportlist')->with('airports', Airport::all())->with('countries', Country::all());
    }
    public function AirportDelete(Request $request){
        Airport::Remove($request);
        return redirect()->back();
    }
    public function AirportEditView(Request $request){
        $Airport = Airport::Detailed($request);

        $GMaps = new Gmaps;
        $config['center'] =  $Airport->location_x. ",".$Airport->location_y;
        $config['zoom'] = '5';
        $GMaps->initialize($config);

        $marker['position'] = $Airport->location_x.",".$Airport->location_y;
        $marker['infowindow_content'] = $Airport->name;
        $GMaps -> add_marker($marker);
        $map = $GMaps->create_map();

        return view('airportedit')->with('airport', $Airport)->with('countries', Country::all())->with('airlines', Airline::all())->with('map', $map);
    }
    public function AirportEdit(Request $request){
        $this -> validate($request, [
            'name' => 'required',
            'airline' => 'required',
        ]);
        Airport::Updates($request);
        return redirect()->back();
    }
    public function AirlineViewList(){
        return view('airlinelist')->with('airlines', Airline::all())->with('countries', Country::all());
    }
    public function AirlineDelete(Request $request){
        Airline::remove($request);
        Airport::AirlineRemove($request);
        return redirect()->back();
    }
    public function AirlineEditView(Request $request){
        $Airline = Airline::Detailed($request);
        return view('airlineedit')->with('airline', $Airline)->with('countries', Country::all());
    }
        public function AirlineEdit(Request $request){
        $this -> validate($request, [
            'name' => 'required',
        ]);
        Airline::Updates($request);
        return redirect()->back();
    }


    public function AirlineView(Request $request){
        return view('airlineform')->with('countries', Country::all());
    }
    public function NewAirline(Request $request){
        $this -> validate($request, [
            'name' => 'required',
        ]);
        Airline::Insert($request);
        return back();

    }
}
