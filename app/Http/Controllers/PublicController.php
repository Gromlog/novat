<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use FarhanWazir\GoogleMaps\GMaps;
use App\Airport;
use App\Country;
use App\Airline;
class PublicController extends Controller
{
    public function index(){
    	return view('home');
    }
    public function map(){
    	$GMaps = new Gmaps;
		$config['center'] = '54.687157, 25.279652';
		$config['zoom'] = '4';
		$GMaps->initialize($config);
        $airports = Airport::all();
        if(count($airports) > 0){
            foreach($airports as $airport){
                $marker['position'] = $airport->location_x.",".$airport->location_y;
                $marker['infowindow_content'] = $airport->name;
                $GMaps -> add_marker($marker);
            }
        }

		$map = $GMaps->create_map();
        return view('map')->with('map', $map)->with('airports', $airports)->with('countries', Country::all())->with('airlines', Airline::all());
    }
    public function info(){
        return view('info')->with('airlines', Airline::all())->with('countries', Country::all())->with('airports', Airport::all());
    }
}
