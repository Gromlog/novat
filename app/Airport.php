<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
	public $timestamps = false;
    public static function Insert($request){
    	$airport = new Airport;
    	$airport->name = $request['name'];
    	$airport->country = $request['country'];
    	$airport->associates = serialize($request['airline']);
    	$airport->location_x = $request['location_x'];
    	$airport->location_y = $request['location_y'];
    	$airport->save();
    }
    public static function Remove($request){
         $remove = Airport::all()->where('id', $request['id'])->first();
         $remove->delete();
    }
    public static function Detailed($request){
        $airport = Airport::all()->where('id', $request['id'])->first();
        return $airport;
    }
    public static function Updates($request){
        $airport = Airport::all()->where('id', $request['id'])->first();
        $airport->name = $request['name'];
        $airport->country = $request['country'];
        $airport->associates = serialize($request['airline']);
        $airport->save();
    }
    public static function AirlineRemove($request){
        $airports = Airport::all();
        foreach($airports as $airport){
            $airline = unserialize($airport->associates);
            if (($key = array_search($request['id'], $airline)) !== false) {
                unset($airline[$key]);
                $airport->associates = serialize($airline);
                $airport->save();
            }
        }
    }
}
