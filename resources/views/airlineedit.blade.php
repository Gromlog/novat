@extends('layouts.app')

@section('content')
@include('inc.admintab')
<div class = "card">
    <div class = "card-body">
		{!! Form::open(['url' => '/admin/update/airline/update']) !!}
			{{Form::hidden('id', $airline->id)}}
    		<div class = "form-group">
    			{{ Form::label('Name', 'Airline name')}}
    			{{ Form::text('name', $airline->name, ['class' => 'form-control'])}}
			</div>
			<div class = "form-group">
				<select name="country">
					@foreach($countries as $country)
						@if($country->id == $airline -> country)
		 					<option value="{{ $country->id}}"selected>{{ $country->name}}</option>
		 				@else
		 					<option value="{{ $country->id}}">{{ $country->name}}</option>
						@endif
					@endforeach
				</select>
			</div>
		{{ Form::submit('Update', ['class' => 'btn btn-primary'])}}
		{!! Form::close()!!}
	</div>
</div>
@endsection