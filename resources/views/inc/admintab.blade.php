<ul class="nav nav-pills">
  <li class="nav-item ">
    <a class="nav-link {{Request::is('admin/new/airport')? 'active': ''}}" href="/admin/new/airport">Register Airport</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{Request::is('admin/new/airline')? 'active': ''}}" href="/admin/new/airline">Register Airline</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{Request::is('admin/update/airportlist')? 'active': ''}}" href="/admin/update/airportlist">Airport Control</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{Request::is('admin/update/airlinelist')? 'active': ''}}" href="/admin/update/airlinelist">Airline Control</a>
  </li>
</ul>