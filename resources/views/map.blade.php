@extends('layouts.app')

@section('content')
	@include('inc.map')
	<h1 class = "text-center">Airport list</h1>
	<table class = "table">
	<thead>
		<tr>
			<th>Airport</th>
			<th>Country</th>
			<th>Associated with</th>
		</tr>
	</thead>
	<tbody>
		@foreach($airports as $airport)
		<tr>
			<td>{{$airport->name}}</td>
			<td>
				@foreach($countries as $country)
					@if($country->id === $airport->country)
						{{$country->name}}
						({{$country->iso}})
					@endif		
				@endforeach
			</td>
			<td>
				@foreach($airlines as $airline) 
					@foreach(unserialize($airport->associates) as $associates)
						@if($associates == $airline->id)
							{{$airline->name}}
							{{","}}

						@endif
					@endforeach
				@endforeach
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection