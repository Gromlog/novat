@extends('layouts.app')
@section('content')
@include('inc.admintab')

<div class = "card">
    <div class = "card-body">
		{!! Form::open(['url' => '/admin/new/airport/submit']) !!}
			<h1 class = "text-center">New Airport</h1>
    		<div class = "form-group">
    			{{ Form::label('Name', 'Airport name')}}
    			{{ Form::text('name', "", ['class' => 'form-control'])}}
			</div>
			<div class = "form-group">
			   <select name="country">
        			@foreach($countries as $country)
         				<option value="{{ $country->id}}">{{ $country->name}}</option>
        			@endforeach
    			</select>
			</div>
      <h4>Most recent marker will be it's coordinates</h4>
			@include('inc.map')</br>
			<div class = "form-group">
				@foreach($airlines as $airline) 
  				{{ Form::label('airline[]', $airline->name)}}
  				{{ Form::checkbox('airline[]', $airline->id)}}
				@endforeach
			</div>
			<div class = "form-group">
    			{{ Form::label('location_x', ' ')}}
    			{{ Form::hidden('location_x', "", ['class' => 'form-control'])}}
			</div>
			<div class = "form-group">
    			{{ Form::label('location_y',' ')}}
    			{{ Form::hidden('location_y', "", ['class' => 'form-control'])}}
			</div>
			{{ Form::submit('Add', ['class' => 'btn btn-primary'])}}
		{!! Form::close()!!}
	</div>
</div>

<script>
      var labels = 'A';
      function initialize() {
        var bangalore = { lat: 54.687157, lng: 25.279652 };
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 6,
          center: bangalore
        });

        // This event listener calls addMarker() when the map is clicked.
        google.maps.event.addListener(map, 'click', function(event) {      
          addMarker(event.latLng, map);

        });
      }
      function addMarker(location, map) { 
        var marker = new google.maps.Marker({
          position: location,
          label: labels,
          map: map
        });

      var lat = marker.getPosition().lat();
	    var lng = marker.getPosition().lng();
		  document.getElementById('location_x').setAttribute('value', lat);
		  document.getElementById('location_y').setAttribute('value', lng);
	}


      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection