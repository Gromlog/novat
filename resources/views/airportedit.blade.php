@extends('layouts.app')

@section('content')
@include('inc.admintab')
<h1 class = "text-center">Edit Airport</h1>
<div class = "card">
    <div class = "card-body">
		{!! Form::open(['url' => '/admin/update/airport/update']) !!}
			{{Form::hidden('id', $airport->id)}}
    		<div class = "form-group">
    			{{ Form::label('Name', 'Airport name')}}
    			{{ Form::text('name', $airport->name, ['class' => 'form-control'])}}
			</div>

			<div class = "form-group">
			   <select name="country">
        			@foreach($countries as $country)
        				@if($country->id === $airport -> country)
         					<option value="{{ $country->id}}"selected>{{ $country->name}}</option>
         				@else
         					<option value="{{ $country->id}}">{{ $country->name}}</option>
     					@endif
        			@endforeach
    			</select>
			</div>
			@include('inc.map')
			<div class = "form-group">
				<?php $found = false;?>
				@foreach($airlines as $airline) 
					@foreach(unserialize($airport->associates) as $associates)
						<?php $found = false;?>
						@if($associates == $airline->id)
							{{ Form::label('airline[]', $airline->name)}}
							{{ Form::checkbox('airline[]', $airline->id, ['type' => 'checked'])}}
							<?php $found = true;?>
							@break
						@endif
					@endforeach
						@if($found === false)
							{{ Form::label('airline[]', $airline->name)}}
							{{ Form::checkbox('airline[]', $airline->id)}}
						@endif
				@endforeach
			</div>
		{{ Form::submit('Update', ['class' => 'btn btn-primary'])}}
		{!! Form::close()!!}
	</div>
</div>
@endsection