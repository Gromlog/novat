@extends('layouts.app')

@section('content')
@include('inc.admintab')
<table class = "table">
	<thead>
		<tr>
			<th>Airline</th>
			<th>Country</th>
			<th>Control</th>
		</tr>
	</thead>
	<tbody>
		@foreach($airlines as $airline)
		<tr>
			<td>{{$airline->name}}</td>
			@foreach($countries as $country)
				@if($country->id == $airline->country)
					<td>{{$country->name}}</td>
				@endif
			@endforeach
			<td>
				{!! Form::open(['url' => '/admin/update/airlinelist/delete']) !!}
					{{ Form::hidden('id', $airline->id)}}
					{{ Form::submit('Delete',['class' => 'btn btn-link'])}}
				{!! Form::close() !!}
				<a href = "/admin/update/airlinelist/edit/{{$airline->id}}" class = "btn btn-link">Edit</a>

        	</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection