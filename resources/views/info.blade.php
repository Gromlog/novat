@extends('layouts.app')

@section('content')
<div class = "row">
	<div class = "col-md-6">
		<h3 class = "text-center">Countries without airlines</h3>
		<ul class = "list-group">
			@foreach($countries as $country)
				<?php $bool = false;?>
				@foreach($airlines as $airline)
					@if($country->id == $airline -> country)
						<?php $bool = true;?>
					@endif
				@endforeach
				@if($bool == false)
					<li class = "list-group-item list-group-item-light">{{$country->name}} ({{$country->iso}})</li>
				@endif
			@endforeach
		</ul>
	</div>
	<div class = "col-md-6">
		<h3 class = "text-center">Countries without airports and airlines</h3>
		<ul class = "list-group">
			@foreach($countries as $country)
				<?php $bool = false; ?>
				@foreach($airports as $airport)
					@if($airport -> country == $country -> id)
						<?php $bool = true; ?>
					@endif
				@endforeach
				@if($bool == false)
					@foreach($airlines as $airline)
						@if($airline->country == $country->id)
							<?php $bool = true;?>
						@endif
					@endforeach
					@if($bool == false)
						<li class = "list-group-item list-group-item-light">{{$country->name}} ({{$country->iso}})</li>
					@endif
				@endif
			@endforeach
		</ul>
	</div>
</div>

@endsection