@extends('layouts.app')
@section('content')
@include('inc.admintab')

<div class = "card">
    <div class = "card-body">
		{!! Form::open(['url' => '/admin/new/airline/submit']) !!}
			<h1 class = "text-center">New Airline</h1>

    		<div class = "form-group">
    			{{ Form::label('Name', 'Airport name')}}
    			{{ Form::text('name', "", ['class' => 'form-control'])}}
			</div>
			<div class = "form-group">
			   <select name="country">
        			@foreach($countries as $country)
         				<option value="{{ $country->id}}">{{ $country->name}}</option>
        			@endforeach
    			</select>
			</div>
			{{ Form::submit('Add', ['class' => 'btn btn-primary'])}}
		{!! Form::close()!!}
	</div>
</div>
@endsection