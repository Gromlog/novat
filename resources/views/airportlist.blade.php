@extends('layouts.app')

@section('content')

@include('inc.admintab')

<table class = "table">
	<thead>
		<tr>
			<th>Airport</th>
			<th>Country</th>
			<th>Associated with</th>
			<th>Control</th>
		</tr>
	</thead>
	<tbody>
		@foreach($airports as $airport)
		<tr>
			<td>{{$airport->name}}</td>
			<td>
				@foreach($countries as $country)
					@if($country->id === $airport->country)
						{{$country->name}}
					@endif		
				@endforeach
			</td>
			<td>{{count(unserialize($airport->associates))}} Airline(s)</td>
			<td>
				{!! Form::open(['url' => '/admin/update/airportlist/delete']) !!}
					{{ Form::hidden('id', $airport->id)}}
					{{ Form::submit('Delete',['class' => 'btn btn-link'])}}
				{!! Form::close() !!}

				<a href = "/admin/update/airportlist/edit/{{$airport->id}}" class = "btn btn-link">Edit</a>

        	</td>
		</tr>
		@endforeach
	</tbody>
</table>

@endsection