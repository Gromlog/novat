<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(); // Authorised users only
Route::get('/home', 'AuthController@index')->name('home');

Route::get('/admin/new/airport', 'AuthController@AirportView');
Route::post('/admin/new/airport/submit', 'AuthController@NewAirport');

Route::get('/admin/new/airline', 'AuthController@AirlineView');
Route::post('admin/new/airline/submit', 'AuthController@NewAirline');

Route::get('/admin/update/airportlist', 'AuthController@AirportListView');
Route::post('/admin/update/airportlist/delete', 'AuthController@AirportDelete');
Route::get('/admin/update/airportlist/edit/{id}', 'AuthController@AirportEditView');
Route::post('/admin/update/airport/update', 'AuthController@AirportEdit');

Route::get('/admin/update/airlinelist', 'AuthController@AirlineViewList');
Route::post('/admin/update/airlinelist/delete', 'AuthController@AirlineDelete');
Route::get('/admin/update/airlinelist/edit/{id}', 'AuthController@AirlineEditView');
Route::post('/admin/update/airline/update', 'AuthController@AirlineEdit');

Route::get('/', 'PublicController@index'); // public
Route::get('/map', 'PublicController@map');
Route::get('/info', 'PublicController@info');
