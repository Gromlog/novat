<?php

use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('countries')->insert([
            ['iso' => 'LTU', 'name' => 'Lithuania'],
            ['iso' => 'USA', 'name' => 'America'],
            ['iso' => 'HRV', 'name' => 'Croatia'],
            ['iso' => 'NLD', 'name' => 'Netherlands'],
            ['iso' => 'CAN', 'name' => 'Canada'],
            ['iso' => 'RUS', 'name' => 'Russia'],
        ]);
    }
}
